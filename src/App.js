import React from 'react';
//import logo from './logo.svg';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// pages
import Home from './pages/Home'
import Login from './pages/Login'
import Volunteerreg from './components/Volunteerreg';

function App() {
  return (
    <div className="App">
      <Router>
      <div>
      <div className="navbar-container">
            <div><h1 className="logo"><Link style={{textDecoration:'none'}} to="/">Rebuild Earth</Link></h1></div>
            <ul>
              <li><Link style={{textDecoration:'none'}} to="/login">Login</Link></li>
              <li><Link to="/volunteerreg">Volunteer</Link></li>
            </ul>
        </div>
        {/*
            Add your routes here
            the first matched route will be used
        */}
        <Switch>

          <Route path="/login" exact >
            <Login />
          </Route>
          <Route path="/volunteerreg" exact >
              <Volunteerreg />
          </Route>

          <Route path="/" component="">
            <Home />
          </Route>

        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;
